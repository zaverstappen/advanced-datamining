import math

def linear(a):
    return a

def sign(a):
    if a > 0:
        y_hat = 1.0
    elif a < 0:
        y_hat = -1.0
    else:
        y_hat = 0
    return y_hat

def mean_squared_error(y_hat, y):
    return (y_hat-y)**2

def mean_absolute_error(y_hat, y):
    return abs(y_hat-y)

def derivative(function, delta=0.01):

    def wrapper_derivative(x, *args):
        return (function(x+delta, *args) - function(x-delta, *args))/(2*delta)  # Hier kun je o.a. "function(x)" aanroepen om de afgeleide mee te berekenen

    wrapper_derivative.__name__ = function.__name__ + '’'
    wrapper_derivative.__qualname__ = function.__qualname__ + '’'
    return wrapper_derivative

def tanh(x):
    return math.tanh(x)

def hinge(yhat,y):
    return max(1-(yhat*y),0)


class Neuron():

    def __repr__(self):
        text = f'Neuron(dim={self.dim}, activation={self.activation.__name__}, loss={self.loss.__name__})'
        return text

    def __init__(self, dim, activation=linear, loss=mean_squared_error):
        self.dim = dim
        self.activation = activation
        self.loss = loss
        self.bias = 0
        self.weights = [0.0 for i in range(dim)]

    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d] * xs[i][d]
            yhat = self.activation(a)
            yhats.append(yhat)
        return yhats

    def partial_fit(self, xs, ys, alpha=0.001):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            der_activate = derivative(self.activation)
            der_loss = derivative(self.loss)
            self.bias -= alpha * der_loss(yhat,y)* der_activate(yhat)
            for i in range(self.dim):
                self.weights[i] -= alpha  * der_loss(yhat,y) * der_activate(yhat)* x[i]

    def fit(self, xs, ys, epochs=40):
        for i in range(epochs):
            self.partial_fit(xs, ys)