class Perceptron():

    def __repr__(self):
        text = f'Perceptron(dim={self.dim})'
        return text

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]

    def predict(self, xs):
        yhats = []
        s = 0.0
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d] * xs[i][d]
            if a > 0:
                yhat = 1.0
            elif a < 0:
                yhat = -1.0
            else:
                yhat = 0
            yhats.append(yhat)
        return yhats

    def partial_fit(self, xs, ys):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat-y
            self.bias -= e
            for i in range(self.dim):
                self.weights[i] -= e*x[i]

    def fit(self, xs, ys, epochs=0):
        if epochs==0:
            corrects = False
        while not corrects:
            self.partial_fit(xs, ys)
            yhats = self.predict(xs)
            if yhats == ys:
                corrects = True


class LinearRegression():

    def __repr__(self):
        text = f'LinearRegression(dim={self.dim})'
        return text

    def __init__(self, dim):
        self.dim = dim
        self.bias = 0.0
        self.weights = [0.0 for i in range(dim)]

    def predict(self, xs):
        yhats = []
        for i in range(len(xs)):
            a = self.bias
            for d in range(self.dim):
                a += self.weights[d] * xs[i][d]
            yhats.append(a)
        return yhats

    def partial_fit(self, xs, ys, alpha=0.001):
        yhats = self.predict(xs)
        for x, y, yhat in zip(xs, ys, yhats):
            e = yhat - y
            self.bias -= alpha * e
            for i in range(self.dim):
                self.weights[i] -= alpha * e * x[i]

    def fit(self, xs, ys, epochs=40):
        for i in range(epochs):
            self.partial_fit(xs, ys)