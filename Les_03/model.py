from collections import Counter
from copy import deepcopy
import math
import random


def linear(a):
    return a

def sign(a):
    if a > 0:
        y_hat = 1.0
    elif a < 0:
        y_hat = -1.0
    else:
        y_hat = 0
    return y_hat

def mean_squared_error(y_hat, y):
    return (y_hat-y)**2

def mean_absolute_error(y_hat, y):
    return abs(y_hat-y)

def derivative(function, delta=0.01):

    def wrapper_derivative(x, *args):
        return (function(x+delta, *args) - function(x-delta, *args))/(2*delta)  # Hier kun je o.a. "function(x)" aanroepen om de afgeleide mee te berekenen

    wrapper_derivative.__name__ = function.__name__ + '’'
    wrapper_derivative.__qualname__ = function.__qualname__ + '’'
    return wrapper_derivative

def tanh(x):
    return math.tanh(x)

def hinge(yhat,y):
    return max(1-(yhat*y),0)


class Layer():

    classcounter = Counter()

    def __init__(self, outputs, *, name=None, next=None):
        Layer.classcounter[type(self)] += 1
        if name is None:
            name = f'{type(self).__name__}_{Layer.classcounter[type(self)]}'
        self.inputs = 0
        self.outputs = outputs
        self.name = name
        self.next = next

    def __repr__(self):
        text = f'Layer(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def add(self, next):
        if self.next is None:
            self.next = next
            next.set_inputs(self.outputs)
        else:
            self.next.add(next)

    def set_inputs(self, inputs):
        self.inputs = inputs

    def __add__(self, next):
        result = deepcopy(self)
        result.add(deepcopy(next))
        return result

    def __call__(self, xs):
        raise NotImplementedError('Abstract __call__ method')


    def __getitem__(self, index):
        if index == 0 or index == self.name:
            return self
        if isinstance(index, int):
            if self.next is None:
                raise IndexError('Layer index out of range')
            return self.next[index - 1]
        if isinstance(index, str):
            if self.next is None:
                raise KeyError(index)
            return self.next[index]
        raise TypeError(f'Layer indices must be integers or strings, not {type(index).__name__}')


class InputLayer(Layer):

    def __repr__(self):
        text = f'InputLayer(outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        yhats, ls, gs = self.next(xs,ys, alpha)
        return yhats, ls, gs

    def predict(self, xs):
        yhats, _, _ = self(xs)
        return yhats

    def evaluate(self, xs, ys):
        _, ls, _ = self(xs, ys)
        lmean = sum(ls) / len(ls)
        return  lmean

    def partial_fit(self, xs, ys, alpha=0.001):
        self(xs, ys, alpha)

    def fit(self, xs, ys, epochs=100, alpha=0.001):
        for e in range(epochs):
            self.partial_fit(xs, ys, alpha)

class Dense(Layer):

    def __init__(self, outputs, name=None):
        super().__init__(outputs, name=name, next=None)
        self.bias = [0.0 for o in range(self.outputs)]
        self.weights = [[] for i in range(self.outputs)]

    def __repr__(self):
        text = f'Dense(inputs={self.inputs}, outputs={self.outputs}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def set_inputs(self, inputs):
        super().set_inputs(inputs)
        spread = math.sqrt( 6/ (self.inputs+self.outputs))
        self.weights = [[random.uniform(-spread, spread) for i in range(self.inputs)] for o in range(self.outputs)]

    def __call__(self, xs, ys=None, alpha=None):
        hs = []
        for x in xs:
            h = []
            for o in range(self.outputs):
                h.append(self.bias[o] + sum(self.weights[o][i] * x[i] for i in range(self.inputs)))
            hs.append(h)
        yhats, ls, qs = self.next(hs, ys, alpha)
        if alpha is None:
            gs = None
        else:
            for q, x in zip(qs, xs):
                for o in range(self.outputs):
                    factor = alpha * q[o]
                    self.bias[o] -= factor
                    for i in range(self.inputs):
                        self.weights[o][i] -= factor * x[i]
            gs = [[sum( q[o]*self.weights[o][i] for o in range(self.outputs)) for i in range(self.inputs)] for q in qs]
        return yhats, ls, gs


class Activation(Layer):

    def __init__(self, outputs, activation=linear, name=None):
        super().__init__(outputs, name=name, next=None)
        self.activation = activation
        self.activation_gradient = derivative(activation)

    def __repr__(self):
        text = f'Activation(inputs={self.inputs}, outputs={self.outputs}, activation={self.activation.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        hs = []
        hs = [[self.activation(xi) for xi in x] for x in xs]
        yhats, ls, qs = self.next(hs, ys, alpha)
        if alpha is None:
            gs = None
        else:
            gs = [[ q[o]*self.activation_gradient(x[o]) for o in range(self.outputs)] for x, q in zip(xs, qs)]
        return yhats, ls, gs


class OutputLayer(Layer):

    def __init__(self, loss=mean_squared_error, *, name=None):
        super().__init__(0, name=name, next=None)
        self.loss = loss
        self.loss_gradient = derivative(loss)

    def __repr__(self):
        text = f'OutputLayer(inputs={self.inputs}, loss={self.loss.__name__}, name={repr(self.name)})'
        if self.next is not None:
            text += ' + ' + repr(self.next)
        return text

    def __call__(self, xs, ys=None, alpha=None):
        yhats = xs
        if ys is None:
            ls = None
            gs = None
        else:
            ls = []
            for x, y in zip(xs, ys):
                l = sum( self.loss(x[i], y[i]) for i in range(self.inputs))
                ls.append(l)
            if alpha is None:
                gs = None
            else:
                gs = [[ self.loss_gradient(x[i], y[i]) for i in range(self.inputs)] for x, y in zip(xs, ys)]
        return yhats, ls, gs